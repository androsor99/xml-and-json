package com.androsor99.dao;

import com.androsor99.entity.ConfigFile;
import com.androsor99.exception.DaoException;
import com.androsor99.logging.LogReport;
import com.androsor99.logging.SeverityLabel;
import com.androsor99.util.PropertiesUtil;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ConfigFileFromXmlDao implements Dao<ConfigFile> {

    public final static String CONFIG_FILE_KEY = "config_file";
    private final static String TAG_SUFFIX = "suffix";
    private final static String TAG_PATH_FILES = "filePaths";
    private final static String TAG_PATH = "path";
    private static final ConfigFileFromXmlDao INSTANCE = new ConfigFileFromXmlDao();
    private static final LogReport logReport = LogReport.getInstance();

    @Override
    public ConfigFile create() throws DaoException {
        String path = PropertiesUtil.get(CONFIG_FILE_KEY);
        String fileName = getFileName(path);
        ConfigFile configFile = new ConfigFile();
        try {
            Document document = buildDocument(path);
            Node rootNode = document.getFirstChild();
            buildChildesElement(configFile, rootNode);
            logReport.log(String.format("The %s was successfully read", fileName), SeverityLabel.INFO);
            return configFile;
        } catch (IOException e) {
            logReport.log(String.format("The %s was not read", fileName), SeverityLabel.ERROR, e);
            throw new DaoException(e);
        } catch (SAXException | ParserConfigurationException e) {
            logReport.log(String.format("Parsing file %s error: ", fileName), SeverityLabel.ERROR, e);
            throw new DaoException(e);
        }
    }

    private String getFileName(String path) {
        return Path.of(path).getFileName().toString();
    }

    private Document buildDocument(String path) throws IOException, SAXException, ParserConfigurationException {
        try (InputStream inputStream = new FileInputStream(path)) {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            return builder.parse(inputStream);
        }
    }

    private void buildChildesElement(ConfigFile configFile, Node rootNode) {
        NodeList rootChildes = rootNode.getChildNodes();
        for (int i = 0; i < rootChildes.getLength(); i++) {
            Node node = rootChildes.item(i);
            boolean isElement = node instanceof Element;
            String nodeName = node.getNodeName();
            if (isElement && nodeName.equals(TAG_SUFFIX)) {
                configFile.setSuffix(node.getTextContent());
            }
            if (isElement && nodeName.equals(TAG_PATH_FILES)) {
                List<Path> filePaths = parseNodeList(node);
                configFile.setFilePaths(filePaths);
            }
        }
    }

    private List<Path> parseNodeList(Node filePaths) {
        NodeList elementChildes = filePaths.getChildNodes();
        List<Path> files = new ArrayList<>();
        for (int j = 0; j < elementChildes.getLength(); j++) {
            Node node = elementChildes.item(j);
            boolean isElement = node instanceof Element;
            String nodeName = node.getNodeName();
            if (isElement && nodeName.equals(TAG_PATH)) {
                files.add(Path.of(node.getTextContent()));
            }
        }
        return files;
    }
    
    public static ConfigFileFromXmlDao getInstance() {
        return INSTANCE;
    }
}
