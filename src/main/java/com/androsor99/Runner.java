package com.androsor99;

import com.androsor99.controller.RenamingBySuffixController;
import com.androsor99.exception.ServiceException;
import com.androsor99.logging.LogReport;
import com.androsor99.logging.SeverityLabel;
import com.androsor99.view.PrinterToFileJSON;

public class Runner {

    private static final RenamingBySuffixController controller = RenamingBySuffixController.getInstance();
    private static final PrinterToFileJSON printerToFileJSON = PrinterToFileJSON.getInstance();
    private static final LogReport logReport = LogReport.getInstance();

    public static void main(String[] args) {

        logReport.log("______________________Entering application______________________", SeverityLabel.INFO);
        try {
            controller.doAction();
            logReport.log("Exiting application.", SeverityLabel.INFO);
        } catch (ServiceException e) {
            logReport.log("Application terminated with an error", SeverityLabel.ERROR, e);
        } finally {
            printerToFileJSON.print(logReport);
        }
    }
}
