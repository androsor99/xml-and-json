package com.androsor99.service;

import com.androsor99.exception.ServiceException;

public interface Service<T> {

    T rename() throws ServiceException;
}
