package com.androsor99.service.dto;

import com.androsor99.dao.ConfigFileFromXmlDao;
import com.androsor99.util.PropertiesUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.ZonedDateTime;
import java.util.Map;

@NoArgsConstructor
@Getter
@Setter
public class OutputResult {

    private final String fileConfig = PropertiesUtil.get(ConfigFileFromXmlDao.CONFIG_FILE_KEY);
    private ZonedDateTime dateAndTime;
    private Map<String, String> files;
    public static final String
            FILE_CONFIG = "fileConfig",
            DATA_AND_TIME = "dateAndTime",
            FILES = "files",
            FILE = "file",
            OLD_NAME = "oldName",
            NEW_NAME ="newName";
}
