package com.androsor99.service;

import com.androsor99.dao.ConfigFileFromXmlDao;
import com.androsor99.entity.ConfigFile;
import com.androsor99.exception.DaoException;
import com.androsor99.exception.ServiceException;
import com.androsor99.service.dto.OutputResult;
import com.androsor99.logging.LogReport;
import com.androsor99.logging.SeverityLabel;
import com.androsor99.validator.FileValidator;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class RenamingBySuffixService implements Service<OutputResult> {

    private static final RenamingBySuffixService INSTANCE = new RenamingBySuffixService();
    private static final LogReport logReport = LogReport.getInstance();
    private final ConfigFileFromXmlDao file = ConfigFileFromXmlDao.getInstance();
    private final FileValidator fileValidator = FileValidator.getInstance();

    public OutputResult rename() throws ServiceException {
        try {
            ConfigFile configFile = file.create();
            return setOutputResult(configFile);
        } catch (DaoException e) {
            throw new ServiceException(e);
        }
    }

    private OutputResult setOutputResult(ConfigFile configFile) {
        OutputResult outputResult = new OutputResult();
        outputResult.setDateAndTime(ZonedDateTime.now());
        outputResult.setFiles(toMap(configFile));
        return outputResult;
    }

    private  Map<String, String> toMap(ConfigFile configFile) {
        Map<String, String> oldAndNewFiles = new HashMap<>();
        String suffix = configFile.getSuffix();
        List<Path> files = configFile.getFilePaths();
        for (Path path : files) {
            String oldFileName = getFileName(path);
            String newFileName = addSuffixToName(oldFileName, suffix);
            Path pathWithNewName = Paths.get(path.getParent().toString(), newFileName);
            boolean fileExist = fileValidator.isFileExist(path);
            boolean newFileExist = fileValidator.isFileExist(pathWithNewName);
            if (fileExist && !newFileExist) {
                renameFile(path, newFileName, oldFileName);
                oldAndNewFiles.put(oldFileName, newFileName);
            } else if(newFileExist) {
                logReport.log(String.format("The file \"%s\" is not renamed. Invalid new name \"%s\". A file with the same name already exists.", oldFileName, newFileName), SeverityLabel.INFO);
            } else {
                logReport.log(String.format("The file \"%s\" does not exist", oldFileName), SeverityLabel.INFO);
            }
        }
        return oldAndNewFiles;
    }

    private void renameFile(Path path, String newFileName, String oldFileName) {
        try {
            move(path, newFileName);
            logReport.log(String.format("File \"%s\" renameMessage successfully. New file \"%s\".", oldFileName, newFileName), SeverityLabel.INFO);
        } catch (IOException e) {
            logReport.log(String.format("The file %s has not been renamed", oldFileName), SeverityLabel.ERROR);
        }
    }

    private String getFileName(Path path) {
        return path.getFileName().toString();
    }

    private String addSuffixToName(String name, String suffix) {
        return String.join(suffix, name.substring(0, name.lastIndexOf(".")), name.substring(name.lastIndexOf(".")));
    }

    private void move(Path path, String fileName) throws IOException {
        Files.move(path, path.resolveSibling(fileName), REPLACE_EXISTING);
    }

    public static RenamingBySuffixService getInstance() {
        return INSTANCE;
    }
}
