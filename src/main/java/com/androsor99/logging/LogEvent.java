package com.androsor99.logging;

import lombok.Data;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@Data
public class LogEvent {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
    private String dateAndTime;
    private String message;
    private String severityLabel;
    private String exception;

    public void of(String message, SeverityLabel severityLabel) {
        this.dateAndTime = ZonedDateTime.now().format(DATE_TIME_FORMATTER);
        this.message = message;
        this.severityLabel = severityLabel.toString();
    }

    public void of(String message, SeverityLabel severityLabel, Throwable exception) {
        of(message, severityLabel);
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        exception.printStackTrace(printWriter);
        this.exception = stringWriter.toString();
    }
}
