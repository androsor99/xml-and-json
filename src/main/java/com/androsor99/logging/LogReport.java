package com.androsor99.logging;

import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor()
public class LogReport {

    private static final LogReport INSTANCE = new LogReport();
    private final List<LogEvent> report = new ArrayList<>();

    private void addReport(LogEvent logEvent) {
       this.report.add(logEvent);
    }

    public List<LogEvent> getReport() {
        return this.report;
    }

    public void log(String message, SeverityLabel severityLabel) {
        LogEvent event = new LogEvent();
        event.of(message, severityLabel);
        this.addReport(event);
    }
    public void log(String message, SeverityLabel severityLabel, Throwable ex) {
        LogEvent event = new LogEvent();
       event.of(message, severityLabel, ex);
        this.addReport(event);
    }

    public static LogReport getInstance() {
        return INSTANCE;
    }
}
