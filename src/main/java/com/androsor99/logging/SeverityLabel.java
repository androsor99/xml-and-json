package com.androsor99.logging;

public enum SeverityLabel {

    INFO, DEBUG, ERROR, FATAL
}
