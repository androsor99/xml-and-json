package com.androsor99.mapping;

import com.androsor99.service.dto.OutputResult;
import com.androsor99.logging.LogReport;
import com.androsor99.logging.SeverityLabel;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.time.format.DateTimeFormatter;
import java.util.Map;

import static com.androsor99.service.dto.OutputResult.DATA_AND_TIME;
import static com.androsor99.service.dto.OutputResult.FILE;
import static com.androsor99.service.dto.OutputResult.FILES;
import static com.androsor99.service.dto.OutputResult.FILE_CONFIG;
import static com.androsor99.service.dto.OutputResult.NEW_NAME;
import static com.androsor99.service.dto.OutputResult.OLD_NAME;

public class MapperXML {

    private static final MapperXML INSTANCE = new MapperXML();
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
    private static final LogReport logReport = LogReport.getInstance();

    public Document map(OutputResult result) {
        Document document = null;
        try {
            document = getDocument();
            buildDocumentXML(result, document);
        } catch (ParserConfigurationException e) {
            logReport.log("Document XML doesn't create", SeverityLabel.ERROR, e);
        }
        return document;
    }

    private Document getDocument() throws ParserConfigurationException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        return builder.newDocument();
    }

    private void buildDocumentXML(OutputResult result, Document document) {
        Element root = document.createElement(result.getClass().getSimpleName());
        document.appendChild(root);
        addChildElement(document, FILE_CONFIG, root, result.getFileConfig());
        addChildElement(document, DATA_AND_TIME, root, result.getDateAndTime().format(DATE_TIME_FORMATTER));
        Element files = addChildElement(document, FILES, root, "");
        addListChildElement(result, document, files);
    }

    private void addListChildElement(OutputResult result, Document document, Element files) {
        for(Map.Entry<String, String> fileNames : result.getFiles().entrySet()) {
            Element file = addChildElement(document, FILE, files, "");
            addChildElement(document, OLD_NAME, file, fileNames.getKey());
            addChildElement(document, NEW_NAME, file, fileNames.getValue());
        }
    }

    private Element addChildElement(Document document, String tagName, Element root, String textValue) {
        Element element = document.createElement(tagName);
        root.appendChild(element);
        Text text = document.createTextNode(textValue);
        element.appendChild(text);
        return element;
    }

    public static MapperXML getInstance() {
        return INSTANCE;
    }
}
