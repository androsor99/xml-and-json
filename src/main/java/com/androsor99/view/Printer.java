package com.androsor99.view;

public interface Printer<T> {

    void print(T arg);
}
