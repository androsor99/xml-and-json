package com.androsor99.view;

import com.androsor99.logging.LogReport;
import com.androsor99.util.PropertiesUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;

import java.io.File;

public class PrinterToFileJSON implements Printer<LogReport> {

    private static final PrinterToFileJSON INSTANCE = new PrinterToFileJSON();
    private static final String FILE_JSON_KEY = "output_json";

    @SneakyThrows
    @Override
    public void print(LogReport report) {
        ObjectMapper mapper = new ObjectMapper();
            mapper.writerWithDefaultPrettyPrinter().writeValue(new File(PropertiesUtil.get(FILE_JSON_KEY)), report);
    }

    public static PrinterToFileJSON getInstance() {
        return INSTANCE;
    }
}
