package com.androsor99.view;

import com.androsor99.mapping.MapperXML;
import com.androsor99.service.dto.OutputResult;
import com.androsor99.logging.LogReport;
import com.androsor99.logging.SeverityLabel;
import com.androsor99.util.PropertiesUtil;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.w3c.dom.Document;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PrinterToFileXML implements Printer<OutputResult> {

    private static final PrinterToFileXML INSTANCE = new PrinterToFileXML();
    private static final MapperXML mapperXML = MapperXML.getInstance();
    private static final LogReport logReport = LogReport.getInstance();
    private static final String FILE_XML_KEY = "output_xml";

    @Override
    public void print(OutputResult result) {
        try {
            Document document = mapperXML.map(result);
            transformToFileXML(document);
            logReport.log("Document XML save to file.", SeverityLabel.INFO);
        } catch (TransformerException | FileNotFoundException e) {
            logReport.log("Document XML doesn't save to file.", SeverityLabel.ERROR, e);
        }
    }

    private void transformToFileXML(Document document) throws TransformerException, FileNotFoundException {
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.transform(new DOMSource(document), new StreamResult(new FileOutputStream(PropertiesUtil.get(FILE_XML_KEY))));
    }

    public static PrinterToFileXML getInstance() {
        return INSTANCE;
    }
}
