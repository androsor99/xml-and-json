package com.androsor99.entity;

import lombok.Data;

import java.nio.file.Path;
import java.util.Collections;
import java.util.List;

@Data
public class ConfigFile {

     private String suffix = "";
     private List<Path> filePaths = Collections.emptyList();
}

